#!/bin/bash
set -e
/init-railgun-conf.sh > /etc/railgun/railgun.conf 2>&1
rsyslogd
/usr/bin/rg-listener -config=/etc/railgun/railgun.conf
