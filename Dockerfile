FROM debian:9-slim

MAINTAINER PolyQY <my@geekman.dev>

ENV RG_WAN_PORT 2408
ENV RG_LOG_LEVEL 0
ENV RG_ACT_TOKEN ""
ENV RG_ACT_HOST ""
ENV RG_MEMCACHED_SERVERS "memcached:11211"

RUN apt-get update && \
  apt-get install -y curl gnupg && \
  echo 'deb http://pkg.cloudflare.com/ stretch main' | tee /etc/apt/sources.list.d/cloudflare-main.list && \
  curl -C - https://pkg.cloudflare.com/pubkey.gpg | apt-key add - && \
  apt-get update && \
  apt-get install -y railgun-stable && \
  apt-get purge curl gnupg -y && \
  apt-get autoremove -y &&  \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/*

COPY docker-entrypoint.sh /docker-entrypoint.sh

COPY init-railgun-conf.sh /init-railgun-conf.sh

RUN chmod +x /docker-entrypoint.sh; chmod +x /init-railgun-conf.sh;

EXPOSE 2408

ENTRYPOINT /docker-entrypoint.sh
